# Introduction

This project contains the Eclipse workspaces of the explorations with Camunda and DMN.

## Prerequisites

Make sure you have the following set of tools installed:

- Java JDK 1.8+,
- Apache Maven (optional, if not installed you can use embedded Maven inside Eclipse.)
- A modern web browser (recent Firefox, Chrome or Microsoft Edge will work fine)
- Eclipse integrated development environment (IDE)

## VIL IIT example

In the [Camunda Platform project](https://gitlab.com/discipl/RON/camunda-platform) we installed a Camunda Platform on [https://vil-regels.nl:8443/](https://vil-regels.nl:8443/).

The RegelSpraak specifications of [Individual Income Allowance](https://open-regels.nl/Rapportages/Rapportage_Individuele_Inkomenstoeslag.html) are trasnlated to a DMN which is deployed on the Camunda Platform.

We follow 2 routes for this:

1. Camunda Modeler using deploy current diagrom to Camunda Platform
2. Eclipse IDE with deploy via Apache Tomcat Manager App

### Route 1

Deploy.
![deploy-via-modeler](./images/deployviamodeler.png)

Camunda Platform.
![vil-regels](./images/vil-regels.png)

### Route 2

TBD

## REST API

Finally, we can illustrate deployed DMN via REST API.

![engine-rest-vil-iit](./images/engine-rest-vil-iit.png)

And the `POST /decision-definition/key/{key}/evaluate` request evaluates the latest version of VIL_IIT_Utrecht decision definition.

![evaluate decision definition](./images/EvaluateDMN.png)
