package org.camunda.bpm.vil.dmn;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

@ProcessApplication("VIL IIT App DMN")
public class VILApplication extends ServletProcessApplication
{
  //empty implementation
}